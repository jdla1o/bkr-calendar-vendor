import React from 'react'
import { storiesOf } from '@storybook/react'

import { DragableCalendar, Calendar } from './helpers'
import resources from './helpers/resourceEvents'

storiesOf('Resources', module).add('demo', () => {
  const EventHoverSlot = ({ event }) => {
    return <div>{event.title}</div>
  }

  return (
    <DragableCalendar
      events={resources.events}
      resources={resources.list}
      defaultView="day"
      components={{
        // event: EventSlot,
        eventHoverSlot: EventHoverSlot,
      }}
    />
  )
})
