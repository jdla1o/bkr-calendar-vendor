export default {
  events: [
    {
      title: 'Rencontre',
      resourceId: 'a',
      start: new Date(2020, 8, 9, 6, 0, 0),
      end: new Date(2020, 8, 9, 14, 30, 0),
    },
    {
      title: 'Another Meeting',
      resourceId: 'b',
      start: new Date(2020, 8, 9, 9, 0, 0),
      end: new Date(2020, 8, 9, 16, 30, 0),
    },
    {
      title: 'A',
      resourceId: 'a',
      start: new Date(2020, 8, 9, 8, 30, 0),
      end: new Date(2020, 8, 9, 12, 30, 0),
    },
    {
      title: 'B',
      resourceId: 'b',
      start: new Date(2020, 8, 10, 7, 0, 0),
      end: new Date(2020, 8, 10, 10, 30, 0),
    },
    {
      title: 'C',
      resourceId: 'c',
      start: new Date(2015, 3, 4, 5, 30, 0, 0),
      end: new Date(2015, 3, 4, 10, 30, 0, 0),
    },
  ],

  list: [
    {
      id: 'a',
      title: 'Room A',
    },
    {
      id: 'b',
      title: 'Room B',
    },
    {
      id: 'c',
      title: 'Room C',
    },
  ],
}
