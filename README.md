# React-Bookr-Calendar

Base on [React Big Calendar](https://github.com/intljusticemission/react-bookr-calendar) to cover the purposes of a calendar in bookr vendor admin

## Use and Setup

`yarn add @letsbookr/react-bookr-calendar` or `npm install --save @letsbookr/react-bookr-calendar`

Include `react-bookr-calendar/lib/css/react-bookr-calendar.css` for styles, and make sure your calendar's container
element has a height, or the calendar won't be visible.
