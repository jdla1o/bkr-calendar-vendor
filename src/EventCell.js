import PropTypes from 'prop-types'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import clsx from 'clsx'
import ReactTooltip from 'react-tooltip'
import * as dates from './utils/dates'

class EventCell extends React.Component {
  onHandleSelect = e => {
    let { onSelect, event } = this.props

    ReactTooltip.hide()
    if (onSelect) onSelect(event, e)
  }

  onHandleSelectDouble = e => {
    let { onDoubleClick, event } = this.props
    ReactTooltip.hide()
    if (onDoubleClick) onDoubleClick(event, e)
  }

  render() {
    let {
      style,
      className,
      event,
      selected,
      isAllDay,
      localizer,
      continuesPrior,
      continuesAfter,
      accessors,
      getters,
      children,
      components: {
        event: Event,
        eventWrapper: EventWrapper,
        eventHoverSlot: EventHoverSlot,
      },
      slotStart,
      slotEnd,
      ...props
    } = this.props

    delete props.resizable

    let title = accessors.title(event)
    let end = accessors.end(event)
    let start = accessors.start(event)
    let allDay = accessors.allDay(event)

    let showAsAllDay =
      isAllDay || allDay || dates.diff(start, dates.ceil(end, 'day'), 'day') > 1

    let userProps = getters.eventProp(event, start, end, selected)

    const content = (
      <div className="rbc-event-content">
        {Event ? (
          <Event
            event={event}
            continuesPrior={continuesPrior}
            continuesAfter={continuesAfter}
            title={title}
            isAllDay={allDay}
            localizer={localizer}
            slotStart={slotStart}
            slotEnd={slotEnd}
          />
        ) : (
          title
        )}
      </div>
    )

    const eventIdKey = `event_hover_${Math.random()
      .toString(36)
      .substr(2, 9)}`

    return (
      <EventWrapper {...this.props} type="date">
        <div
          {...props}
          tabIndex={0}
          style={{
            ...userProps.style,
            ...style,
          }}
          className={clsx(
            'rbc-event',
            'rbc-event-with-color',
            className,
            userProps.className,
            {
              'rbc-selected': selected,
              'rbc-event-allday': showAsAllDay,
              'rbc-event-continues-prior': continuesPrior,
              'rbc-event-continues-after': continuesAfter,
            }
          )}
          onClick={e => this.onHandleSelect(e)}
          onDoubleClick={e => this.onHandleSelectDouble(e)}
          data-for={eventIdKey}
          data-tip
        >
          {typeof children === 'function' ? children(content) : content}
          {EventHoverSlot && (
            <ReactTooltip
              id={eventIdKey}
              className="rbc-hover-container"
              html={true}
              getContent={() =>
                ReactDOMServer.renderToString(<EventHoverSlot event={event} />)
              }
            />
          )}
        </div>
      </EventWrapper>
    )
  }
}

EventCell.propTypes = {
  event: PropTypes.object.isRequired,
  slotStart: PropTypes.instanceOf(Date),
  slotEnd: PropTypes.instanceOf(Date),

  resizable: PropTypes.bool,

  selected: PropTypes.bool,
  isAllDay: PropTypes.bool,
  continuesPrior: PropTypes.bool,
  continuesAfter: PropTypes.bool,

  accessors: PropTypes.object.isRequired,
  components: PropTypes.object.isRequired,
  getters: PropTypes.object.isRequired,
  localizer: PropTypes.object,

  onSelect: PropTypes.func,
  onDoubleClick: PropTypes.func,

  key: PropTypes.any,
}

export default EventCell
