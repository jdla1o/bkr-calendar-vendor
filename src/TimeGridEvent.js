import React from 'react'
import ReactDOMServer from 'react-dom/server'
import clsx from 'clsx'
import ReactTooltip from 'react-tooltip'

function stringifyPercent(v) {
  return typeof v === 'string' ? v : v + '%'
}

/* eslint-disable react/prop-types */
function TimeGridEvent(props) {
  const {
    style,
    className,
    event,
    accessors,
    resource,
    rtl,
    selected,
    label,
    continuesEarlier,
    continuesLater,
    getters,
    onClick,
    onDoubleClick,
    components: {
      event: Event,
      eventWrapper: EventWrapper,
      eventHoverSlot: EventHoverSlot,
    },
  } = props
  let title = accessors.title(event)
  let end = accessors.end(event)
  let start = accessors.start(event)

  let userProps = getters.eventProp(event, start, end, selected)

  const eventIdKey = `event_hover_${Math.random()
    .toString(36)
    .substr(2, 9)}`

  const onHandleSelect = e => {
    ReactTooltip.hide()
    onClick(e)
  }

  const onHandleSelectDouble = e => {
    ReactTooltip.hide()
    onDoubleClick(e)
  }

  let { height, top, width, xOffset } = style

  const inner = [
    <div key="1" className="rbc-event-label">
      {label}
    </div>,
    <div key="2" className="rbc-event-content">
      {title}
    </div>,
  ]
  //check if resource have a color
  const hasResourceColor = resource && resource.hasOwnProperty('resourceColor')

  const HoverComponent = () => {
    return ReactDOMServer.renderToString(<EventHoverSlot event={event} />)
  }

  //Add the color to the tag 'div'
  const addColorResourceColor = hasResourceColor && {
    backgroundColor: resource.resourceColor,
  }

  return (
    <EventWrapper type="time" {...props}>
      <div
        onClick={e => onHandleSelect(e)}
        onDoubleClick={e => onHandleSelectDouble(e)}
        style={{
          ...userProps.style,
          ...addColorResourceColor,
          top: stringifyPercent(top),
          [rtl ? 'right' : 'left']: stringifyPercent(xOffset),
          width: stringifyPercent(width),
          height: stringifyPercent(height),
        }}
        data-for={eventIdKey}
        data-tip
        className={clsx('rbc-event', className, userProps.className, {
          'rbc-selected': selected,
          //If resource dont haave a color, show original color
          'rbc-event-with-color': !hasResourceColor,
          'rbc-event-continues-earlier': continuesEarlier,
          'rbc-event-continues-later': continuesLater,
        })}
      >
        {Event ? <Event event={event} title={title} {...props} /> : inner}
        {EventHoverSlot && (
          <ReactTooltip
            id={eventIdKey}
            className="rbc-hover-container"
            html={true}
            getContent={() => HoverComponent()}
          />
        )}
      </div>
    </EventWrapper>
  )
}

export default TimeGridEvent
