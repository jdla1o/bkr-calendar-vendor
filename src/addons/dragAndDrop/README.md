### Drag and Drop

```js
import { Calendar } from 'react-bookr-calendar'
import withDragAndDrop from 'react-bookr-calendar/dist/addons/dragAndDrop'

import 'react-bookr-calendar/dist/addons/dragAndDrop/styles.css'

const DraggableCalendar = withDragAndDrop(Calendar)

/* ... */

return (
  <DraggableCalendar
    localizer={myLocalizer}
    events={events}
    draggableAccessor={event => true}
  />
)
```
