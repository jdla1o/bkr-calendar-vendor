### react-bookr-calendar Addons

Addons are community contributed and maintained additional functionality built on top of the core `react-bookr-calendar`

* [Drag and Drop](./dragAndDrop/README.md)
