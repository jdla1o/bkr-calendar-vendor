import clsx from 'clsx'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import BackgroundWrapper from './BackgroundWrapper'
import { dateIsInBusinessHours } from './utils/helpers'

export default class TimeSlotGroup extends Component {
  render() {
    const {
      renderSlot,
      resource,
      group,
      getters,
      workingHoursResource,
      isSlotDate,
      hasWorkingHours,
      components: { timeSlotWrapper: Wrapper = BackgroundWrapper } = {},
    } = this.props

    const groupProps = getters ? getters.slotGroupProp() : {}

    const renderSlotTime = value => {
      const time = value.toLocaleTimeString([], { timeStyle: 'short' })
      return <span className={clsx('rbc-slot-time-hover')}>{time}</span>
    }

    return (
      <div className="rbc-timeslot-group" {...groupProps}>
        {group.map((value, idx) => {
          const slotProps = getters ? getters.slotProp(value, resource) : {}
          const isDisabled =
            workingHoursResource && workingHoursResource.length > 0
              ? !dateIsInBusinessHours(value, workingHoursResource, true)
              : true

          return (
            <Wrapper key={idx} value={value} resource={resource}>
              <div
                {...slotProps}
                className={clsx(
                  'rbc-time-slot',
                  slotProps.className,
                  !isDisabled && hasWorkingHours && 'rbc-available',
                  isDisabled && hasWorkingHours && 'rbc-disabled'
                )}
              >
                {isSlotDate && !isDisabled && renderSlotTime(value)}
                {renderSlot && renderSlot(value, idx)}
              </div>
            </Wrapper>
          )
        })}
      </div>
    )
  }
}

TimeSlotGroup.propTypes = {
  renderSlot: PropTypes.func,
  group: PropTypes.array.isRequired,
  resource: PropTypes.any,
  workingHoursResource: PropTypes.array,
  components: PropTypes.object,
  getters: PropTypes.object,
  isSlotDate: PropTypes.bool,
  hasWorkingHours: PropTypes.bool,
}
