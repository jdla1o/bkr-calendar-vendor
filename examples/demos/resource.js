import React, { useEffect } from 'react'
import { Calendar, Views } from 'react-bookr-calendar'
import ExampleControlSlot from '../ExampleControlSlot'

const events = [
  {
    id: 0,
    title: 'Board meeting',
    start: new Date(2020, 8, 9, 6, 0, 0),
    end: new Date(2020, 8, 9, 14, 30, 0),
    resourceId: 2,
    color: '#000',
  },
  {
    id: 1,
    title: 'MS training',
    start: new Date(2020, 8, 9, 9, 0, 0),
    end: new Date(2020, 8, 9, 16, 30, 0),
    resourceId: 2,
  },
  {
    id: 2,
    title: 'Team lead meeting',
    start: new Date(2020, 8, 9, 8, 30, 0),
    end: new Date(2020, 8, 9, 12, 30, 0),
    resourceId: 3,
  },
  {
    id: 11,
    title: 'Birthday Party',
    start: new Date(2020, 8, 10, 7, 0, 0),
    end: new Date(2020, 8, 10, 10, 30, 0),
    resourceId: 4,
  },
]

const resourceMap = [
  { resourceId: 1, resourceTitle: 'Board room' },
  { resourceId: 2, resourceTitle: 'Training room' },
  { resourceId: 3, resourceTitle: 'Meeting room 1' },
  { resourceId: 4, resourceTitle: 'Meeting room 2' },
  // { resourceId: 4, resourceTitle: 'Meeting room 2', resourceColor: '#f00' },
]

const workingHours = [
  {
    start: '14:00',
    end: '16:00',
    resourceId: 1,
  },
]

// const EventSlot = ({ event, styles, className }) => {
//   return (
//     <div style={styles} className={className}>
//       {event.title}
//     </div>
//   )
// }

const EventHoverSlot = ({ event }) => {
  console.count('EventHoverSlot')
  return <div>{event.title}</div>
}

let Resource = ({ localizer }) => (
  <>
    <Calendar
      events={events}
      localizer={localizer}
      defaultView={Views.DAY}
      views={['day', 'work_week', 'month']}
      step={60}
      defaultDate={new Date(2018, 0, 29)}
      resources={resourceMap}
      workingHours={workingHours}
      resourceIdAccessor="resourceId"
      resourceTitleAccessor="resourceTitle"
      onSelectSlot={() => console.log('onSelectSlot')}
      onSelectEvent={() => console.log('onSelectEvent')}
      selectable
      components={{
        // event: EventSlot,
        eventHoverSlot: EventHoverSlot,
      }}
    />
  </>
)

export default Resource
